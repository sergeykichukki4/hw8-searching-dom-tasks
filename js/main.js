"use strict";

const paragraphs = document.querySelectorAll("p");
paragraphs.forEach((p) => p.style.backgroundColor = "#FF0000")
console.log(paragraphs);

const byId = document.querySelector("#optionsList");
console.log(byId);
for (let key of byId.children) {
    console.log(key.nodeType, key.nodeName);
}

const byClass = document.querySelectorAll(".testParagraph");
byClass.forEach((el) => el.textContent = "This is a paragraph");
console.log(byClass);

const nestedElems = document.querySelectorAll(".main-header > *");
nestedElems.forEach((el) => el.classList.add("nav-item"));
console.log(nestedElems);

const titleElems = document.querySelectorAll(".section-title");
console.log(titleElems);
titleElems.forEach((del) => del.classList.remove("section-title")); //there are no section titles in prime HTML, but that`s how I will solve it
console.log(titleElems);




